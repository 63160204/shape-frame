/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.shapeproject04;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author patthamawan
 */
public class SquareFrame {
    public static void main(String[] args) {
        final JFrame frame = new JFrame("Square");
        frame.setSize(330, 330);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);
        
        JLabel lblSide = new JLabel("Side:", JLabel.TRAILING);
        lblSide.setSize(60, 20);
        lblSide.setLocation(5, 5);
        lblSide.setBackground(Color.WHITE);
        lblSide.setOpaque(true);
        frame.add(lblSide);
        
        final JTextField txtSide = new JTextField();
        txtSide.setSize(60, 20);
        txtSide.setLocation(65, 5);
        frame.add(txtSide);
        
        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(130, 5);
        frame.add(btnCalculate);
        
        final JLabel lblResult = new JLabel("Square= ??? area= ??? perimeter= ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(330, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.MAGENTA);
        lblResult.setOpaque(true);
        frame.add(lblResult);
        // Event Driven
        btnCalculate.addActionListener(new ActionListener(){ // Anonymous class
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                String strSide = txtSide.getText();
                double Side = Double.parseDouble(strSide);
                Square square = new Square(Side);
                lblResult.setText("Square= " + String.format("%.2f",square.getSide()) 
                        + " area = " + String.format("%.2f", square.calArea())
                        + " perimeter = " + String.format("%.2f", square.calPerimeter()));
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(frame, "Error: Please input number"
                            , "Error", JOptionPane.ERROR_MESSAGE);
                    txtSide.setText("");
                    txtSide.requestFocus();
                }
            }
        });
        
        frame.setVisible(true);
    }
}

