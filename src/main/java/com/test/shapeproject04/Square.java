/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.shapeproject04;

/**
 *
 * @author patthamawan
 */
public class Square extends Shape {

    private double Side;

    public Square(double Side) {
        super("Square");
        this.Side = Side;
    }

    public double getSide() {
        return Side;
    }
    
    public void setSide(double Side) {
        this.Side = Side;
    }
    
    @Override
    public double calArea() {
        return Side*Side;
    }

    @Override
    public double calPerimeter() {
        return Side*4;
    }

}
