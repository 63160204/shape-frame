/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.shapeproject04;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author patthamawan
 */
public class CircleFrame2 extends JFrame{
    JLabel lblRadius;
    JTextField txtRadius;
    JButton btnCalculate;
    JLabel lblResult;
    public CircleFrame2(){
        super("Circle");
        this.setSize(330, 330);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);
        
        lblRadius = new JLabel("radius:", JLabel.TRAILING);
        lblRadius.setSize(60, 20);
        lblRadius.setLocation(5, 5);
        lblRadius.setBackground(Color.WHITE);
        lblRadius.setOpaque(true);
        this.add(lblRadius);
        
        txtRadius = new JTextField();
        txtRadius.setSize(60, 20);
        txtRadius.setLocation(65, 5);
        this.add(txtRadius);
        
        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(130, 5);
        this.add(btnCalculate);
        
        lblResult = new JLabel("Circle radius= ??? area= ??? perimeter= ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(330, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.MAGENTA);
        lblResult.setOpaque(true);
        this.add(lblResult);
        // Event Driven
        btnCalculate.addActionListener(new ActionListener(){ // Anonymous class
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                //1.ดึงข้อมูล text จาก  txtRadius -> strRadius
                String strRadius = txtRadius.getText();
                //2.แปลง strRadius -> radius:double parseDouble
                double radius = Double.parseDouble(strRadius);
                //3.instance object Cricle(redius)-> cricle
                Circle circle = new Circle(radius);
                //4.update lblRadius โดยนำข้อมูลจาก cricle ไปแสดงให้ครบถ้วน
                lblResult.setText("Circle r = " + String.format("%.2f",circle.getRadius()) 
                        + " area = " + String.format("%.2f", circle.calArea())
                        + " perimeter = " + String.format("%.2f", circle.calPerimeter()));
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(CircleFrame2.this, "Error: Please input number"
                            , "Error", JOptionPane.ERROR_MESSAGE);
                    txtRadius.setText("");
                    txtRadius.requestFocus();
                }
            }
        });
    }
    public static void main(String[] args) {
        CircleFrame2 frame = new CircleFrame2();
        frame.setVisible(true);
        
    }
}
