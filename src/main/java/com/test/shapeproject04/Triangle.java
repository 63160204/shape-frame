/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.shapeproject04;

/**
 *
 * @author patthamawan
 */
public class Triangle extends Shape{
    private double side;
    private double base;

    public Triangle(double side, double base) {
        super("Triangle");
        this.side = side;
        this.base = base;
    }

    public double getSide() {
        return side;
    }

    public void getBase() {
        this.base = base;
    }
    
    public void setSide(double side) {
        this.side = side;
    }
    
    public void setBase(double base) {
        this.base = base;
    }
    
    @Override
    public double calArea() {
        return 0.5*side*base;
    }

    @Override
    public double calPerimeter() {
        return side+side+base;
    }

}
