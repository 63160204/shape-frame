/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.shapeproject04;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author patthamawan
 */
public class RectangleFrame {
    public static void main(String[] args) {
        final JFrame frame = new JFrame("Rectangle");
        frame.setSize(400, 400);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);
        
        JLabel lblwidth = new JLabel("width:", JLabel.TRAILING);
        lblwidth.setSize(60, 20);
        lblwidth.setLocation(5, 5);
        lblwidth.setBackground(Color.WHITE);
        lblwidth.setOpaque(true);
        frame.add(lblwidth);
        
        JLabel lbllongs = new JLabel("longs:", JLabel.TRAILING);
        lbllongs.setSize(60, 20);
        lbllongs.setLocation(5, 20);
        lbllongs.setBackground(Color.WHITE);
        lbllongs.setOpaque(true);
        frame.add(lbllongs);
        
        final JTextField txtwidth = new JTextField();
        txtwidth.setSize(60, 20);
        txtwidth.setLocation(65, 5);
        frame.add(txtwidth);
        
        final JTextField txtlongs = new JTextField();
        txtlongs.setSize(60, 20);
        txtlongs.setLocation(65, 25);
        frame.add(txtlongs);
        
        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(130, 5);
        frame.add(btnCalculate);
        
        final JLabel lblResult = new JLabel("Rectangle = ??? area= ??? perimeter= ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(400, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.MAGENTA);
        lblResult.setOpaque(true);
        frame.add(lblResult);
        // Event Driven
        btnCalculate.addActionListener(new ActionListener(){ // Anonymous class
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                String strwidth = txtwidth.getText();
                String strlongs = txtlongs.getText();
                double width = Double.parseDouble(strwidth);
                double longs = Double.parseDouble(strlongs);
                Rectangle rectangle = new Rectangle(width,longs);
                lblResult.setText("Rectangle= " + String.format("%.2f",rectangle.getwidth()) 
                        +" area = "+ String.format("%.2f", rectangle.calArea())
                        + " perimeter = " + String.format("%.2f", rectangle.calPerimeter()));
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(frame, "Error: Please input number"
                            , "Error", JOptionPane.ERROR_MESSAGE);
                    txtwidth.setText("");
                    txtlongs.setText("");
                    txtwidth.requestFocus();
                    
                }
            }
        });
        
        frame.setVisible(true);
    }
}
