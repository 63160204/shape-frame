/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.shapeproject04;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author patthamawan
 */
public class CircleFrame {
    public static void main(String[] args) {
        final JFrame frame = new JFrame("Cricle");
        frame.setSize(330, 330);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);
        
        JLabel lblRadius = new JLabel("radius:", JLabel.TRAILING);
        lblRadius.setSize(60, 20);
        lblRadius.setLocation(5, 5);
        lblRadius.setBackground(Color.WHITE);
        lblRadius.setOpaque(true);
        frame.add(lblRadius);
        
        final JTextField txtRadius = new JTextField();
        txtRadius.setSize(60, 20);
        txtRadius.setLocation(65, 5);
        frame.add(txtRadius);
        
        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(130, 5);
        frame.add(btnCalculate);
        
        final JLabel lblResult = new JLabel("Circle radius= ??? area= ??? perimeter= ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(330, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.MAGENTA);
        lblResult.setOpaque(true);
        frame.add(lblResult);
        // Event Driven
        btnCalculate.addActionListener(new ActionListener(){ // Anonymous class
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                //1.ดึงข้อมูล text จาก  txtRadius -> strRadius
                String strRadius = txtRadius.getText();
                //2.แปลง strRadius -> radius:double parseDouble
                double radius = Double.parseDouble(strRadius);
                //3.instance object Cricle(redius)-> cricle
                Circle circle = new Circle(radius);
                //4.update lblRadius โดยนำข้อมูลจาก cricle ไปแสดงให้ครบถ้วน
                lblResult.setText("Circle r = " + String.format("%.2f",circle.getRadius()) 
                        + " area = " + String.format("%.2f", circle.calArea())
                        + " perimeter = " + String.format("%.2f", circle.calPerimeter()));
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(frame, "Error: Please input number"
                            , "Error", JOptionPane.ERROR_MESSAGE);
                    txtRadius.setText("");
                    txtRadius.requestFocus();
                }
            }
        });
        
        frame.setVisible(true);
    }
}
