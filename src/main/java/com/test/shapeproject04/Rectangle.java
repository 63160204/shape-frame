/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.shapeproject04;

/**
 *
 * @author patthamawan
 */
public class Rectangle  extends Shape {

    private double width;
    private double longs;

    public Rectangle(double width, double longs) {
        super("Rectangle");
        this.width = width;
        this.longs = longs;
    }

    public double getwidth() {
        return width;
    }

    public void getlongs() {
        this.longs = longs;
    }
    
    public void setwidth(double width) {
        this.width = width;
    }
    
    public void setlongs(double longs) {
        this.longs = longs;
    }
    
    @Override
    public double calArea() {
        return width*longs;
    }

    @Override
    public double calPerimeter() {
        return 2*(width*longs);
    }

}
