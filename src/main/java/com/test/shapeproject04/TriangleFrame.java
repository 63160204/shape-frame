/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.shapeproject04;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author patthamawan
 */
public class TriangleFrame {
    public static void main(String[] args) {
        final JFrame frame = new JFrame("Triangle");
        frame.setSize(400, 400);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);
        
        JLabel lblSide = new JLabel("side:", JLabel.TRAILING);
        lblSide.setSize(60, 20);
        lblSide.setLocation(5, 5);
        lblSide.setBackground(Color.WHITE);
        lblSide.setOpaque(true);
        frame.add(lblSide);
        
        JLabel lblBase = new JLabel("base:", JLabel.TRAILING);
        lblBase.setSize(60, 20);
        lblBase.setLocation(5, 20);
        lblBase.setBackground(Color.WHITE);
        lblBase.setOpaque(true);
        frame.add(lblBase);
        
        final JTextField txtSide = new JTextField();
        txtSide.setSize(60, 20);
        txtSide.setLocation(65, 5);
        frame.add(txtSide);
        
        final JTextField txtBase = new JTextField();
        txtBase.setSize(60, 20);
        txtBase.setLocation(65, 25);
        frame.add(txtBase);
        
        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(130, 5);
        frame.add(btnCalculate);
        
        final JLabel lblResult = new JLabel("Triangle = ??? area= ??? perimeter= ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(400, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.MAGENTA);
        lblResult.setOpaque(true);
        frame.add(lblResult);
        // Event Driven
        btnCalculate.addActionListener(new ActionListener(){ // Anonymous class
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                String strSide = txtSide.getText();
                String strBase = txtBase.getText();
                double side = Double.parseDouble(strSide);
                double base = Double.parseDouble(strBase);
                Triangle triangle = new Triangle(side,base);
                lblResult.setText("Rectangle= " + String.format("%.2f",triangle.getSide()) 
                        +" area = "+ String.format("%.2f", triangle.calArea())
                        + " perimeter = " + String.format("%.2f", triangle.calPerimeter()));
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(frame, "Error: Please input number"
                            , "Error", JOptionPane.ERROR_MESSAGE);
                    txtSide.setText("");
                    txtBase.setText("");
                    txtSide.requestFocus();
                    
                }
            }
        });
        
        frame.setVisible(true);
    }
}
